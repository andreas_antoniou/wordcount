﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using WordCount.AlgorithmImplementations;
using WordCount.Infrastructure;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;

namespace WordCount
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly AlgorithmExecutorService _executorService;

        private ObservableCollection<AvailableAlgorithms> _algorithms;
        private AvailableAlgorithms _selectedAlgorithm;
        private bool _autoDetectAlgorithm;
        private bool _useAllAlgorithms;
        private string _inputString;
        private string _inputFile;
        private bool _isBusy;
        private ObservableCollection<ExecutionResult> _results;
        private IDictionary<string, CountWrapper> _wordCounts;
        private int _uniqueWordCount;
        private int _totalWordCount;
        private string _loadingMessage;

        private ICommand _runCommand;
        private ICommand _cancelCommand;
        private ICommand _pickFileCommand;
        private ICommand _exportToCsvCommand;

        public MainViewModel()
        {
            _executorService = new AlgorithmExecutorService();

            InputString = "Go do that thing that you do so well";

            Algorithms = new ObservableCollection<AvailableAlgorithms>(_executorService.AllAlgorithms);

            RunCommand = new DelegateCommand(_ => OnRun());
            CancelCommand = new DelegateCommand(_ => OnCancel());
            PickFileCommand = new DelegateCommand(_ => OnPickFileCommand());
            ExportToCsvCommand = new DelegateCommand(_ => OnExportToXmlCommand());
        }

        public ObservableCollection<AvailableAlgorithms> Algorithms
        {
            get { return _algorithms; }
            set { SetProperty(ref _algorithms, value); }
        }

        public AvailableAlgorithms SelectedAlgorithm
        {
            get { return _selectedAlgorithm; }
            set { SetProperty(ref _selectedAlgorithm, value); }
        }

        public bool AutoDetectAlgorithm
        {
            get { return _autoDetectAlgorithm; }
            set { SetProperty(ref _autoDetectAlgorithm, value); }
        }

        public bool UseAllAlgorithms
        {
            get { return _useAllAlgorithms; }
            set
            {
                SetProperty(ref _useAllAlgorithms, 
                    value,
                    () =>
                    {

                    });
            }
        }

        public string InputString
        {
            get { return _inputString; }
            set
            {
                SetProperty(ref _inputString,
                    value,
                    () =>
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            InputFile = string.Empty;
                        }
                    });
            }
        }

        public string InputFile
        {
            get { return _inputFile; }
            set
            {
                SetProperty(ref _inputFile,
                    value,
                    () =>
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            InputString = string.Empty;
                        }
                    });
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        public ObservableCollection<ExecutionResult> Results
        {
            get { return _results; }
            set { SetProperty(ref _results, value); }
        }

        public IDictionary<string, CountWrapper> WordCounts
        {
            get { return _wordCounts; }
            set { SetProperty(ref _wordCounts, value); }
        }

        public int UniqueWordCount
        {
            get { return _uniqueWordCount; }
            set { SetProperty(ref _uniqueWordCount, value); }
        }

        public int TotalWordCount
        {
            get { return _totalWordCount; }
            set { SetProperty(ref _totalWordCount, value); }
        }

        public string LoadingMessage
        {
            get { return _loadingMessage; }
            set { SetProperty(ref _loadingMessage, value); }
        }

        public ICommand RunCommand
        {
            get { return _runCommand; }
            set { SetProperty(ref _runCommand, value); }
        }

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
            set { SetProperty(ref _cancelCommand, value); }
        }

        public ICommand PickFileCommand
        {
            get { return _pickFileCommand; }
            set { SetProperty(ref _pickFileCommand, value); }
        }

        public ICommand ExportToCsvCommand

        {
            get { return _exportToCsvCommand; }
            set { SetProperty(ref _exportToCsvCommand, value); }
        }

        private void OnRun()
        {
            IsBusy = true;
            LoadingMessage = "Executing Word Count...";

            Task.Factory
                .StartNew(() =>
                {
                    if (!string.IsNullOrEmpty(InputString))
                    {
                        return _executorService.Run(GatherExecutionOptions(), InputString);
                    }

                    return _executorService.RunFromFile(GatherExecutionOptions(), InputFile);
                })
                .ContinueWith(t =>
                {
                    Results = new ObservableCollection<ExecutionResult>(t.Result);

                    if (Results.Count == 0)
                    {
                        WordCounts = new Dictionary<string, CountWrapper>();
                        UniqueWordCount = 0;
                        TotalWordCount = 0;
                    }
                    else
                    {
                        WordCounts = Results[0].WordCounts;
                        UniqueWordCount = WordCounts.Count();
                        TotalWordCount = WordCounts.Sum(wc => wc.Value.Count);
                    }

                    IsBusy = false;

                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void OnCancel()
        {
            IsBusy = false;
        }

        private void OnPickFileCommand()
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Sample Texts");
                fileDialog.Filter = "Text Files (.txt)|*.txt";

                var result = fileDialog.ShowDialog();

                switch (result)
                {
                    case DialogResult.OK:
                        InputFile = fileDialog.FileName;
                        break;

                    case DialogResult.Cancel:
                    default:
                        InputFile = string.Empty;
                        break;
                }
            }
        }

        private void OnExportToXmlCommand()
        {
            IsBusy = true;
            LoadingMessage = "Exporting to Xml...";

            Task.Factory
                .StartNew(() =>
                {
                    var export = new XDocument();

                    var rootElement = new XElement("Words");
                    export.Add(rootElement);

                    foreach (var wordCount in WordCounts.OrderBy(wc => wc.Key))
                    {
                        rootElement.Add(
                            new XElement("Word",
                                new XAttribute("value", wordCount.Key),
                                new XAttribute("count", wordCount.Value)));
                    }

                    if (File.Exists("WordCounts.xml"))
                    {
                        File.Delete("WordCounts.xml");
                    }
                    export.Save("WordCounts.xml");
                })
                .ContinueWith(t =>
                {
                    MessageBox.Show("Word counts exported to running directory in file WordCounts.xml.", "Results Exported", MessageBoxButtons.OK);

                    IsBusy = false;

                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private ExecuteOptions GatherExecutionOptions()
        {
            if (AutoDetectAlgorithm)
            {
                return new ExecuteOptions { AutoDetectAlgorithm = true };
            }

            if (UseAllAlgorithms)
            {
                return new ExecuteOptions
                {
                    AlgorithmsToExecute = _executorService.AllAlgorithms
                };
            }

            return new ExecuteOptions { AlgorithmsToExecute = new[] { SelectedAlgorithm } };
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value, Action propertyChangedAction = null, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return false;
            }

            storage = value;

            OnPropertyChanged(propertyName);

            if (propertyChangedAction != null)
            {
                propertyChangedAction.Invoke();
            }

            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}