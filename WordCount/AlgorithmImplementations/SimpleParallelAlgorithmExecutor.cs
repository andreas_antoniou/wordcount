﻿using System.Collections.Concurrent;
using System.IO;

namespace WordCount.AlgorithmImplementations
{
    /// <summary>
    /// This executor has got a known bug where case insensitivity is not adhered to.
    /// </summary>
    public class SimpleParallelAlgorithmExecutor : BaseSimpleAlgorithmExecutor, IAlgorithmExecutor
    {
        protected override AvailableAlgorithms ImplementsAlgorithm { get { return AvailableAlgorithms.SimpleMultithreading; } }
        public ExecutionResult Run(string text)
        {
            return DoRun(text);
        }

        public ExecutionResult RunFromFile(string textFilePath)
        {
            if (!File.Exists(textFilePath))
            {
                throw new FileNotFoundException(textFilePath);
            }

            string text = File.ReadAllText(textFilePath);

            return DoRun(text);
        }

        private ExecutionResult DoRun(string text)
        {
            var wordCountDictionary = new ConcurrentDictionary<string, CountWrapper>();

            return DoRun(text,
                wordCountDictionary,
                splitArray =>
                {
                    foreach (var item in splitArray)
                    {
                        CountWrapper itemCount;
                        if (wordCountDictionary.TryGetValue(item, out itemCount))
                        {
                            itemCount.Count++;
                        }
                        else
                        {
                            wordCountDictionary.TryAdd(item, new CountWrapper());
                        }
                    }
                });
        }
    }
}