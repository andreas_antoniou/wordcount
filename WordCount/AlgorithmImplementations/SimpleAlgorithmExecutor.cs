﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WordCount.AlgorithmImplementations
{
    public class SimpleAlgorithmExecutor : BaseSimpleAlgorithmExecutor, IAlgorithmExecutor
    {
        protected override AvailableAlgorithms ImplementsAlgorithm { get { return AvailableAlgorithms.Simple; } }

        public ExecutionResult Run(string text)
        {
            return DoRun(text);
        }

        public ExecutionResult RunFromFile(string textFilePath)
        {
            if (!File.Exists(textFilePath))
            {
                throw new FileNotFoundException(textFilePath);
            }

            string text = File.ReadAllText(textFilePath);

            return DoRun(text);
        }

        private ExecutionResult DoRun(string text)
        {
            var wordCountDictionary = new Dictionary<string, CountWrapper>(StringComparer.OrdinalIgnoreCase);

            return DoRun(text,
                wordCountDictionary,
                splitArray =>
                {
                    foreach (var item in splitArray)
                    {
                        CountWrapper itemCount;
                        if (wordCountDictionary.TryGetValue(item, out itemCount))
                        {
                            itemCount.Count++;
                        }
                        else
                        {
                            wordCountDictionary.Add(item, new CountWrapper());
                        }
                    }
                });
        }
    }
}