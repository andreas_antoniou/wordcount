﻿using System.Collections.Generic;

namespace WordCount.AlgorithmImplementations
{
    public interface IAlgorithmExecutor
    {
        ExecutionResult Run(string text);
        ExecutionResult RunFromFile(string textFilePath);
    }
}