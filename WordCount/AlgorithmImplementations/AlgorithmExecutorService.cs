﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WordCount.AlgorithmImplementations
{
    public class AlgorithmExecutorService
    {
        private readonly IDictionary<AvailableAlgorithms, IAlgorithmExecutor> _algorithms;

        public AlgorithmExecutorService()
        {
            _algorithms = new Dictionary<AvailableAlgorithms, IAlgorithmExecutor>
            {
                { AvailableAlgorithms.Simple, new SimpleAlgorithmExecutor() },
                { AvailableAlgorithms.SimpleMultithreading, new SimpleParallelAlgorithmExecutor() },
                { AvailableAlgorithms.Linq, new LinqAlgorithmExecutor() },
                { AvailableAlgorithms.TrieStructure, new TrieStructureExecutor() },
            };
        }

        public IEnumerable<AvailableAlgorithms> AllAlgorithms
        {
            get
            {
                return new[]
                {
                    AvailableAlgorithms.Simple,
                    AvailableAlgorithms.SimpleMultithreading,
                    AvailableAlgorithms.Linq,
                    AvailableAlgorithms.TrieStructure
                };
            }
        }

        public static string[] GetDelimiters()
        {
            return new[]
            {
                " ", ".", ",", "!", "?", ":", ";", "\"", "’", "-", "(", ")", "#", "£", "$", "*", "_", "'", "―",
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "“", "&", "%", "–", "”", "[", "]", "/", "‘",
                Environment.NewLine
            };
        }

        public IList<ExecutionResult> Run(ExecuteOptions options, string text)
        {
            var result = new List<ExecutionResult>();

            if (options.AutoDetectAlgorithm)
            {
                // Cannot really handle massive text in textbox so just accept simple algorithm
                options.AlgorithmsToExecute = new[] { AvailableAlgorithms.Simple };
            }

            foreach(var algorithmToExecute in options.AlgorithmsToExecute)
            {
                var executor = _algorithms[algorithmToExecute];
                result.Add(executor.Run(text));
            }

            return result;
        }

        public IList<ExecutionResult> RunFromFile(ExecuteOptions options, string textFilePath)
        {
            var result = new List<ExecutionResult>();

            if (options.AutoDetectAlgorithm)
            {
                options.AlgorithmsToExecute = new[] { AutoDetectOptimalAlgorithm(textFilePath) };
            }

            foreach (var algorithmToExecute in options.AlgorithmsToExecute)
            {
                var executor = _algorithms[algorithmToExecute];
                result.Add(executor.RunFromFile(textFilePath));
            }

            return result;
        }

        private AvailableAlgorithms AutoDetectOptimalAlgorithm(string textFilePath)
        {
            if (!File.Exists(textFilePath))
            {
                throw new FileNotFoundException(textFilePath);
            }

            var fileInfo = new FileInfo(textFilePath);

            return (ConvertBytesToMegabytes(fileInfo.Length) > 100)
                ? AvailableAlgorithms.TrieStructure
                : AvailableAlgorithms.Simple;
            
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
}