﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace WordCount.AlgorithmImplementations
{
    public class TrieStructureExecutor : IAlgorithmExecutor
    {
        public static readonly CharCaseInsensitiveComparer CharComparer = new CharCaseInsensitiveComparer();
        public ExecutionResult Run(string text)
        {
            var stopWatch = Stopwatch.StartNew();

            var rootNodes = new Dictionary<char, TrieNode>(CharComparer);

            var textSplitByLine = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            foreach(var line in textSplitByLine)
            {
                var splitArray = line.Split(AlgorithmExecutorService.GetDelimiters(), StringSplitOptions.RemoveEmptyEntries);
                RunLine(rootNodes, splitArray);
            }

            var wordCountDictionary = PerfomTotalWordCount(rootNodes);

            stopWatch.Stop();

            return new ExecutionResult(AvailableAlgorithms.TrieStructure,
                stopWatch.Elapsed,
                wordCountDictionary);
        }

        public ExecutionResult RunFromFile(string textFilePath)
        {
            if (!File.Exists(textFilePath))
            {
                throw new FileNotFoundException(textFilePath);
            }

            var stopWatch = Stopwatch.StartNew();

            var rootNodes = new Dictionary<char, TrieNode>(CharComparer);
            
            using (var fileStream = new FileStream(textFilePath, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        var splitArray = line.Split(AlgorithmExecutorService.GetDelimiters(), StringSplitOptions.RemoveEmptyEntries);
                        RunLine(rootNodes, splitArray);
                    }
                }
            }

            var wordCountDictionary = PerfomTotalWordCount(rootNodes);

            stopWatch.Stop();

            return new ExecutionResult(AvailableAlgorithms.TrieStructure,
                stopWatch.Elapsed,
                wordCountDictionary);
        }

        private void RunLine(Dictionary<char, TrieNode> rootNodes, string[] splitArray)
        {
            foreach (var item in splitArray)
            {
                char keyChar = item[0];

                TrieNode rootNode;

                if (!rootNodes.TryGetValue(keyChar, out rootNode))
                {
                    rootNode = new TrieNode(keyChar, 0, item);
                    rootNodes.Add(keyChar, rootNode);
                }
                else
                {
                    rootNode.AppendToTree(1, item);
                }
            }
        }

        private Dictionary<string, CountWrapper> PerfomTotalWordCount(Dictionary<char, TrieNode> rootNodes)
        {
            var wordCountDictionary = new Dictionary<string, CountWrapper>(StringComparer.OrdinalIgnoreCase);

            foreach (var rootNode in rootNodes)
            {
                PerformNodeWordCount(rootNode.Value, wordCountDictionary);
            }

            return wordCountDictionary;
        }

        private void PerformNodeWordCount(TrieNode node, Dictionary<string, CountWrapper> wordCountDictionary)
        {
            if (node.WordCount > 0)
            {
                CountWrapper count;
                if (wordCountDictionary.TryGetValue(node.Word, out count))
                {
                    count.Count += node.WordCount;
                }
                else
                {
                    wordCountDictionary.Add(node.Word, new CountWrapper { Count = node.WordCount });
                }
            }

            if (node.Children != null)
            {
                foreach(var childNode in node.Children)
                {
                    PerformNodeWordCount(childNode.Value, wordCountDictionary);
                }
            }
        }

        private class TrieNode
        {
            public char Key { get; private set; }
            public string Word { get; private set; }
            public int WordCount { get; set; }
            public IDictionary<char, TrieNode> Children { get; private set; }

            public TrieNode(char key, int keyCharIndex, string word)
            {
                Key = key;
                Word = word.Substring(0, keyCharIndex + 1);
                Children = new Dictionary<char, TrieNode>(CharComparer);

                if (word.Length == keyCharIndex + 1)
                {
                    WordCount = 1;
                }
                else
                {
                    int nextKeyCharIndex = keyCharIndex + 1;
                    char nextKey = word[nextKeyCharIndex];

                    Children.Add(nextKey, new TrieNode(nextKey, nextKeyCharIndex, word));
                }
            }

            public void AppendToTree(int keyCharIndex, string word)
            {
                if (word.Length == keyCharIndex)
                {
                    WordCount++;
                    return;
                }

                char key = word[keyCharIndex];

                TrieNode childNode;

                if (!Children.TryGetValue(key, out childNode))
                {
                    childNode = new TrieNode(key, keyCharIndex, word);
                    Children.Add(key, childNode);
                }
                else
                {
                    if (word.Length == keyCharIndex + 1)
                    {
                        childNode.WordCount++;
                    }
                    else
                    {
                        childNode.AppendToTree(keyCharIndex + 1, word);
                    }
                }
            }
        }

        public class CharCaseInsensitiveComparer : IEqualityComparer<char>
        {
            public bool Equals(char x, char y)
            {
                return char.ToUpperInvariant(x) == char.ToUpperInvariant(y);
            }

            public int GetHashCode(char obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}