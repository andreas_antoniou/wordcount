﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace WordCount.AlgorithmImplementations
{
    public abstract class BaseSimpleAlgorithmExecutor
    {
        protected abstract AvailableAlgorithms ImplementsAlgorithm { get; }

        protected ExecutionResult DoRun(string text,
            IDictionary<string, CountWrapper> wordCountDictionary,
            Action<string[]> action)
        {
            var stopWatch = Stopwatch.StartNew();

            var splitArray = text.Split(AlgorithmExecutorService.GetDelimiters(), StringSplitOptions.RemoveEmptyEntries);

            action.Invoke(splitArray);

            stopWatch.Stop();

            return new ExecutionResult(ImplementsAlgorithm,
                stopWatch.Elapsed,
                wordCountDictionary);
        }
    }
}