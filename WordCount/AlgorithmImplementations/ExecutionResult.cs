﻿using System;
using System.Collections.Generic;

namespace WordCount.AlgorithmImplementations
{
    public class ExecutionResult
    {
        public ExecutionResult(AvailableAlgorithms algorithmUsed,
            TimeSpan executionTimeSpan,
            IDictionary<string, CountWrapper> wordCounts)
        {
            AlgorithmUsed = algorithmUsed;
            ExecutionTimeSpan = executionTimeSpan;
            WordCounts = wordCounts;
        }

        public AvailableAlgorithms AlgorithmUsed { get; set; }
        public TimeSpan ExecutionTimeSpan { get; set; }
        public IDictionary<string, CountWrapper> WordCounts { get; set; }
    }
}