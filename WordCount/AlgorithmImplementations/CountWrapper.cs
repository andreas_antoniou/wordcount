﻿namespace WordCount.AlgorithmImplementations
{
    public class CountWrapper
    {
        public CountWrapper()
        {
            Count = 1;
        }
        public int Count { get; set; }

        public override string ToString()
        {
            return Count.ToString();
        }
    }
}