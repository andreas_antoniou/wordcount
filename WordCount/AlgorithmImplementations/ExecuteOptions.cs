﻿using System.Collections.Generic;

namespace WordCount.AlgorithmImplementations
{
    public class ExecuteOptions
    {
        public IEnumerable<AvailableAlgorithms> AlgorithmsToExecute { get; set; }
        public bool AutoDetectAlgorithm { get; set; }
    }
}