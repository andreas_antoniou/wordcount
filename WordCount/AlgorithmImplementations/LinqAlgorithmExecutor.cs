﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WordCount.AlgorithmImplementations
{
    public class LinqAlgorithmExecutor : IAlgorithmExecutor
    {
        public ExecutionResult Run(string text)
        {
            return DoRun(text);
        }

        public ExecutionResult RunFromFile(string textFilePath)
        {
            if (!File.Exists(textFilePath))
            {
                throw new FileNotFoundException(textFilePath);
            }

            string text = File.ReadAllText(textFilePath);

            return DoRun(text);
        }

        private ExecutionResult DoRun(string text)
        {
            var stopWatch = Stopwatch.StartNew();

            var wordCountDictionary = text.Split(AlgorithmExecutorService.GetDelimiters(), StringSplitOptions.RemoveEmptyEntries)
                                          .GroupBy(i => i, StringComparer.OrdinalIgnoreCase)
                                          .ToDictionary(i => i.Key, i => new CountWrapper() { Count = i.Count() });

            stopWatch.Stop();

            return new ExecutionResult(AvailableAlgorithms.Linq,
                stopWatch.Elapsed,
                wordCountDictionary);
        }
    }
}