﻿using System.ComponentModel;

namespace WordCount
{
    public enum AvailableAlgorithms
    {
        [Description("Simple")]
        Simple,

        [Description("Simple - Multithreading")]
        SimpleMultithreading,

        [Description("Linq")]
        Linq,

        [Description("Trie Structure")]
        TrieStructure
    }
}